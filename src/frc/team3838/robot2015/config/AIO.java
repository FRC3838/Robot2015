package frc.team3838.robot2015.config;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@SuppressWarnings("PointlessArithmeticExpression")
public final class AIO
{

    public static final int GYRO = 0;



    public static final Map<Integer, String> mappings;
    private static final Logger logger = LoggerFactory.getLogger(AIO.class);


    static
    {
        logger.info("Mapping AIO assignments and checking for duplicates");
        mappings = new HashMap<>();
        try
        {
            final Class<AIO> clazz = AIO.class;
            final Field[] fields = clazz.getFields();

            for (Field field : fields)
            {
                if (int.class.isAssignableFrom(field.getType()))
                {
                    final int value = field.getInt(null);
                    if (value != -1)
                    {
                        final String oldValue = mappings.put(value, field.getName());
                        if (oldValue != null)
                        {
                            final String msg = String.format(">>>>>FATAL ERROR : AIO port # %s is duplicated! It is assigned to both '%s' and '%s'", value, oldValue, field.getName());
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            logger.error(msg);
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            throw new ExceptionInInitializerError(msg);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.warn("Could not validate the AIO fields due to an Exception. Cause Details: {}", e.toString(), e);
        }

    }


    private AIO() { }


    public static void init()
    {
        // a no op method to have the AIO class initialize
    }
}
