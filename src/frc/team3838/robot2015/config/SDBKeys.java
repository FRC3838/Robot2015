package frc.team3838.robot2015.config;

public final class SDBKeys
{
    public static final String AVG_DISTANCE = " Average Distance ";

    public static final String LEFT_DISTANCE = " Left Distance ";
    public static final String RIGHT_DISTANCE = " Right Distance ";
    public static final String SIDE_MOVEMENT_DISTANCE = "Side Movement";

    public static final String FRONT_RIGHT_DISTANCE = " F Right ";
    public static final String FRONT_LEFT__DISTANCE = " F Left  ";
    public static final String REAR__RIGHT_DISTANCE = " R Right ";
    public static final String REAR__LEFT__DISTANCE = " R Left  ";

    public static final String DRIVE_CURVE = "Drive Curve Factor";

    public static final String ELEVATOR_HEIGHT = " Elevator Height ";
    public static final String ELEVATOR_HEIGHT_MAX = " Elevator Max Height ";
    public static final String ELEVATOR_HEIGHT_MIN = " Elevator Min Height ";

    public static final String CLAW_STATUS = " Claw Status ";

    public static final String WINCH_LOWER_LIMIT_SWITCH = " Lower Limit Sw ";
    public static final String WINCH_UPPER_LIMIT_SWITCH = " Upper Limit Sw ";
    public static final String WINCH_LOWER_LIMIT_SWITCH_Alt = " L ";
    public static final String WINCH_UPPER_LIMIT_SWITCH_Alt = " U ";

    public static final String WINCH_SPEED = " Winch speed ";
    public static final String GYRO_ANGLE = " Gyro Angle ";


    private SDBKeys() { }
}
