package frc.team3838.robot2015.config;

import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.joda.time.Interval;
import org.joda.time.ReadableInstant;



public class MatchDates
{

    public static final DateTime firstDay = new DateTime(2015, 3, 26, 0, 0).minus(Hours.hours(12));
    public static final DateTime lastDay = new DateTime(2015, 3, 28, 23, 59).plus(Hours.hours(12));
    public static final Interval matchInterval = new Interval(firstDay, lastDay);

    public static boolean isDuringMatch(ReadableInstant readableInstant)
    {
        return matchInterval.contains(readableInstant);
    }
}
