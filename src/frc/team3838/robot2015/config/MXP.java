package frc.team3838.robot2015.config;

/**
 * Class to store configuration for the My Expansion Port (MXP) on the roboRIO.
 */
public final class MXP
{
    public static final int PORT_ADD_FACTOR = 10;

    private MXP() { }
}
