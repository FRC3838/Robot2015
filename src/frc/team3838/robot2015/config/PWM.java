package frc.team3838.robot2015.config;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public final class PWM
{
    public static final int TEST_MOTOR = -1;

    public static final int WINCH_MOTOR = 5;

    public static final int FRONT_LEFT__WHEEL = 4;
    public static final int FRONT_RIGHT_WHEEL = 3 ;
    public static final int REAR__LEFT__WHEEL = 1;
    public static final int REAR__RIGHT_WHEEL = 2;


    public static final Map<Integer, String> mappings;

    private static final Logger logger = LoggerFactory.getLogger(PWM.class);


    static
    {
        logger.info("Mapping PWMChannels assignments and checking for duplicates");
        mappings = new HashMap<>();
        try
        {
            final Class<PWM> clazz = PWM.class;
            final Field[] fields = clazz.getFields();

            for (Field field : fields)
            {
                if (int.class.isAssignableFrom(field.getType()))
                {
                    final int value = field.getInt(null);
                    if (value != -1)
                    {
                        final String oldValue = mappings.put(value, field.getName());
                        if (oldValue != null)
                        {
                            final String msg = String.format(">>>>>FATAL ERROR : PWM Channel # %s is duplicated! It is assigned to both '%s' and '%s'", value, oldValue, field.getName());
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            logger.error(msg);
                            logger.error(">>>>>FATAL ERROR<<<<<");
                            throw new ExceptionInInitializerError(msg);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.warn("Could not validate the PWM fields due to an Exception. Cause Details: {}", e.toString(), e);
        }

    }


    private PWM() { }


    public static void init()
    {
        // a no op method to have the PWMChannels class initialize
    }

}
