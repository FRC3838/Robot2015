package frc.team3838.robot2015;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.controls.Joysticks;
import frc.team3838.robot2015.config.AIO;
import frc.team3838.robot2015.config.DIO;
import frc.team3838.robot2015.config.PCM;
import frc.team3838.robot2015.config.PWM;



/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI2015
{

    @SuppressWarnings("UnusedDeclaration")
    private static final Logger logger = LoggerFactory.getLogger(OI2015.class);


    public OI2015()
    {
        initChannelAssignments();
        Joysticks.init();

        initTestMotor();
        initTestComponentsSubsystem();
        initWinchSubsystem();
    }


    private void initChannelAssignments()
    {
        AIO.init();
        DIO.init();
        PCM.init();
        PWM.init();
    }


    private void initTestMotor()
    {
        if (EM.isTestMotorSubsystemEnabled)
        {
            logger.debug("Starting OI2015.initTestMotor()");
            try
            {
                logger.debug("Exiting OI2015.initTestMotor()");
            }
            catch (Exception e)
            {
                logger.debug("An exception occurred in OI2015.initTestMotor()", e);
            }
        }
        else
        {
            logger.debug("testMotorSubsystem is NOT enabled and will not be initialized.");
        }

    }


    private void initWinchSubsystem()
    {
        if (EM.isWinchSubsystemEnabled)
        {
            logger.debug("Starting OI2015.initWinchSubsystem()");
            try
            {



                logger.debug("Exiting OI2015.initWinchSubsystem()");


            }
            catch (Exception e)
            {
                logger.error("An exception occurred in OI2015.initWinchSubsystem()", e);
            }
        }
        else
        {
            logger.debug("winchSubsystem is NOT enabled and will not be initialized.");
        }

    }



    private void initTestComponentsSubsystem()
    {
        if (EM.isTestComponentsSubsystemEnabled)
        {
            try
            {
                logger.debug("Starting OI2015.initTestComponentsSubsystem()");



            }
            catch (Exception e)
            {
                logger.error("An exception occurred in OI2015.initTestMotor()", e);
            }
        }
        else
        {
            logger.debug("testComponentsSubsystem is NOT enabled and will not be initialized.");
        }
    }

}

