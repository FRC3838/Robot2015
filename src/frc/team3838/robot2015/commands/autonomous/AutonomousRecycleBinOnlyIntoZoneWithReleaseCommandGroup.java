package frc.team3838.robot2015.commands.autonomous;

import frc.team3838.robot2015.commands.claw.ClawOpenCommand;
import frc.team3838.robot2015.commands.winch.WinchLowerCommand;



public class AutonomousRecycleBinOnlyIntoZoneWithReleaseCommandGroup extends AutonomousRecycleBinOnlyIntoZoneNoReleaseCommandGroup
{
    @Override
    protected void addCommands()
    {
        super.addCommands();
        /*
            1 - 5 from AutonomousIntermediateNoReleaseCommandGroup
            6) Lower same amt raised (1.5 feet)
            7) Open Claw
         */

        addSequential(WinchLowerCommand.feet(1.5));
        addSequential(new ClawOpenCommand());
    }
}
