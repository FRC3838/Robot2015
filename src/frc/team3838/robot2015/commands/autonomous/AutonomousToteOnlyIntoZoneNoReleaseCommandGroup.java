package frc.team3838.robot2015.commands.autonomous;


import frc.team3838.controls.standardCommands.SleepCommand;
import frc.team3838.robot2015.commands.claw.ClawCloseCommand;
import frc.team3838.robot2015.commands.driveTrain.drive.DriveSetDistance;
import frc.team3838.robot2015.commands.driveTrain.drive.RotateRobotCommand;

import static frc.team3838.controls.standardCommands.SleepCommand.PartialSeconds.QuarterSecond;



public class AutonomousToteOnlyIntoZoneNoReleaseCommandGroup extends AutonomousAbstractCommandGroup
{
    public AutonomousToteOnlyIntoZoneNoReleaseCommandGroup() { super(); }


    public AutonomousToteOnlyIntoZoneNoReleaseCommandGroup(String name) { super(name); }


    @Override
    protected void addCommands()
    {
        /*
            TOTE ONLY
                1) Close Claw
                2) Wait
                3) Raise 2 inches

                4) Drive Back 6.5 feet
                5) Rotate 90 Degrees (CW)
         */

        addSequential(new ClawCloseCommand());
        addSequential(SleepCommand.createSeconds(1, QuarterSecond));
        //addSequential(WinchRaiseCommand.inches(2));

        addSequential(DriveSetDistance.reverseFeet(7.0));
        addSequential(RotateRobotCommand.clockwise90Degrees());

    }

}