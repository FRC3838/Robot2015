package frc.team3838.robot2015.commands.autonomous;

import edu.wpi.first.wpilibj.command.Command;



public class LogCommandGroupStartCommand extends AbstractLogCommandGroupStatusCommand
{

    public LogCommandGroupStartCommand(Command commandToLog)
    {
        super(commandToLog);
    }


    @Override
    protected String getStatus() {return "STARTING";}


}
