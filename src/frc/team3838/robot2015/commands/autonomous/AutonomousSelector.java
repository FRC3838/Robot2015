package frc.team3838.robot2015.commands.autonomous;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;



public class AutonomousSelector
{
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(AutonomousSelector.class);

    public static final SendableChooser autoModeChooser = new SendableChooser();

    private static final Command robotOnly = new AutonomousRobotOnlyIntoZoneCommandGroup();
    private static final Command toteOnlyNoRelease = new AutonomousToteOnlyIntoZoneNoReleaseCommandGroup();
    private static final Command recycleBinOnlyNoRelease = new AutonomousRecycleBinOnlyIntoZoneNoReleaseCommandGroup();
    private static final Command recycleBinOnlyWithRelease = new AutonomousRecycleBinOnlyIntoZoneWithReleaseCommandGroup();
    private static final Command binAndTote = new AutonomousBinAndToteIntoZoneCommandGroup();
    private static final Command doNothingCommand = new AutonomousBinAndToteIntoZoneCommandGroup();

    public static Command selectCommand()
    {
        return (Command) autoModeChooser.getSelected();
    }


    public static void setupChooser()
    {
        autoModeChooser.addDefault("1. Robot Only into Zone (Default)", robotOnly);
        autoModeChooser.addObject("2. Tote Only into Zone", toteOnlyNoRelease);
        autoModeChooser.addObject("3. Recycle Bin Only into Zone - NO Release", recycleBinOnlyNoRelease);
        autoModeChooser.addObject("4. Recycle Bin Only into Zone - With Release", recycleBinOnlyWithRelease);
        autoModeChooser.addObject("5. Recycle Bin & Tote into Zone", binAndTote);
        autoModeChooser.addObject("6. No Autonomous Mode (Do Nothing)", doNothingCommand);
        SmartDashboard.putData("Autonomous Mode Chooser ", autoModeChooser);
    }
}
