package frc.team3838.robot2015.commands.autonomous;

import frc.team3838.robot2015.commands.CommandBase;



public class AutonomousDefaultCommand extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AutonomousDefaultCommand.class);


    public AutonomousDefaultCommand()
    {
//        requires(subsystem);
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        logger.debug("{}.initialize() called", getClass().getSimpleName());
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            // No op
        }
        else
        {
            logger.info("Not all required subsystems for AutonomousDefaultCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                logger.debug("{}.execute() called", getClass().getSimpleName());
                logger.info(">>SIMULATED AUTONOMOUS COMMAND EXECUTION: {} <<", getClass().getSimpleName());
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in AutonomousDefaultCommand.execute()", e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for AutonomousDefaultCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return true;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        //return (subsystem.isEnabled());
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
