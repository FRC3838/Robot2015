package frc.team3838.robot2015.commands.autonomous;


import frc.team3838.controls.standardCommands.SleepCommand;
import frc.team3838.robot2015.commands.claw.ClawCloseCommand;
import frc.team3838.robot2015.commands.claw.ClawOpenCommand;
import frc.team3838.robot2015.commands.driveTrain.drive.DriveInArcCommandGroup;
import frc.team3838.robot2015.commands.driveTrain.drive.DriveSetDistance;
import frc.team3838.robot2015.commands.driveTrain.drive.DriveStopCommand;
import frc.team3838.robot2015.commands.driveTrain.drive.RotateRobotCommand;
import frc.team3838.robot2015.commands.winch.WinchLowerCommand;
import frc.team3838.robot2015.commands.winch.WinchLowerFullyCommand;
import frc.team3838.robot2015.commands.winch.WinchRaiseCommand;

import static frc.team3838.controls.standardCommands.SleepCommand.PartialSeconds.EighthSecond;
import static frc.team3838.controls.standardCommands.SleepCommand.PartialSeconds.HalfSecond;



public class AutonomousBinAndToteIntoZoneCommandGroup extends AutonomousAbstractCommandGroup
{
    public AutonomousBinAndToteIntoZoneCommandGroup() { super(); }


    public AutonomousBinAndToteIntoZoneCommandGroup(String name) { super(name); }

    @Override
    protected void addCommands()
    {
        /*
            1) Close Claw
            2) Lift 2 feet -need to verify the height
            3) Arc to the left

            4) Down 6.75" - to place barrel on tote
            5) Open Claw
            6) Arms Lower x - to get arms at height of the tote

            7) Close the claw
                may need to add some forward movement
              Wait for claw
            8) Raise 8 inches
            9) [Arc into the zone] = Matt says its sideways - but that may not allow us to get over the bump, so we are going to try
                A) Rotate 90 CCW
                B) Drive forward 6.5 feet
                C) Rotate 90 CW - may need to tweak the number of degrees
         */

        // @formatter:off
        /* 1*/ addSequential(new ClawCloseCommand());
               addSequential(SleepCommand.createSeconds(1, EighthSecond));
        /* 2*/ addSequential(WinchRaiseCommand.feet(2));
        /* 3*/ addSequential(new DriveInArcCommandGroup());
               addSequential(new DriveStopCommand());

        /* 4*/ addSequential(WinchLowerCommand.inches(6.75));
        /* 5*/ addSequential(new ClawOpenCommand());

        /* 6*/ addSequential(new WinchLowerFullyCommand());
               addSequential(SleepCommand.createSeconds(0, HalfSecond));

        /* 7*/ addSequential(new ClawCloseCommand());
               addSequential(SleepCommand.createSeconds(1, EighthSecond));
        /* 8*/ addSequential(WinchRaiseCommand.inches(12));


        /* 9*/ addSequential(RotateRobotCommand.counterclockwise(100));
               addSequential(SleepCommand.createSeconds(0, EighthSecond));
               addSequential(DriveSetDistance.forwardFeet(7.5));
               addSequential(RotateRobotCommand.clockwise90Degrees());

         // @formatter:on
    }
}