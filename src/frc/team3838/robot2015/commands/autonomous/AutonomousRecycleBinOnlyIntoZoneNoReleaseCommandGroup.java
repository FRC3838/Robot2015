package frc.team3838.robot2015.commands.autonomous;


import frc.team3838.controls.standardCommands.SleepCommand;
import frc.team3838.robot2015.commands.claw.ClawCloseCommand;
import frc.team3838.robot2015.commands.driveTrain.drive.DriveSetDistance;
import frc.team3838.robot2015.commands.driveTrain.drive.RotateRobotCommand;
import frc.team3838.robot2015.commands.winch.WinchRaiseCommand;

import static frc.team3838.controls.standardCommands.SleepCommand.PartialSeconds.QuarterSecond;



public class AutonomousRecycleBinOnlyIntoZoneNoReleaseCommandGroup extends AutonomousAbstractCommandGroup
{

    public AutonomousRecycleBinOnlyIntoZoneNoReleaseCommandGroup()
    {
        super();
    }


    public AutonomousRecycleBinOnlyIntoZoneNoReleaseCommandGroup(String name)
    {
        super(name);
    }


    @Override
    protected void addCommands()
    {
        /*
            RECYCLE BIN OLY - no Release
                1) Close Claw
                2) Wait for claw to close
                3) Lift 1.5 foot

                4) Drive back 6 feet
                5) Rotate 90 Degrees (CW)
         */

        addSequential(new ClawCloseCommand());
        addSequential(SleepCommand.createSeconds(1, QuarterSecond));
        addSequential(WinchRaiseCommand.feet(1.5));

        addSequential(DriveSetDistance.reverseFeet(7.0));
        addSequential(RotateRobotCommand.clockwise90Degrees());

    }
}