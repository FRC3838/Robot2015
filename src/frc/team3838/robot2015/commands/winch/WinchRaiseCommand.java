package frc.team3838.robot2015.commands.winch;

import frc.team3838.robot2015.commands.CommandBase;
import frc.team3838.robot2015.subsystems.WinchSubsystem;



public class WinchRaiseCommand extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(WinchRaiseCommand.class);

    private boolean isFirstCall = true;

    private double targetHeight;

    private final double inchesToRaise;


    public static WinchRaiseCommand feet(double feetToRaise)
    {
        logger.trace("static feet factory called with value of {}", feetToRaise);
        return new WinchRaiseCommand(feetToRaise * 12);
    }


    public static WinchRaiseCommand inches(double inchesToRaise)
    {
        logger.trace("static inches factory called with value of {}", inchesToRaise);
        return new WinchRaiseCommand(inchesToRaise);
    }

    private WinchRaiseCommand(double inchesToRaise)
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(winchSubsystem);
        this.inchesToRaise = inchesToRaise;
        logger.trace("received {} inchesToRaise and set this.inchesToRaise to {}", inchesToRaise, this.inchesToRaise);
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            //no op
        }
        else
        {
            logger.info("Not all required subsystems for WinchRaiseCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                logger.trace("Winch Raise Command called. isFirstCall = {}; currentHeight = {}", isFirstCall, winchSubsystem.getHeightInInches());
                if (isFirstCall)
                {
                    final double currentHeight = winchSubsystem.getHeightInInches();
                    final double targetHeight = currentHeight + inchesToRaise;
                    this.targetHeight = Math.min(targetHeight, WinchSubsystem.MAX_UP_HEIGHT);
                    logger.debug("{} first execution with a requested delta of {} inches. Current Height = {}; Target Height calculated as {} and minimized to {}",
                                 getClass().getSimpleName(), inchesToRaise, currentHeight, targetHeight, this.targetHeight);
                    isFirstCall = false;
                }

                if (isFinished())
                {
                    winchSubsystem.stop();
                    logger.debug("{}.execute() - isFinished returned true, stopping winch", getClass().getSimpleName());
                }
                else
                {

                    final double remainingDistance = targetHeight - winchSubsystem.getHeightInInches();
                    final double speed;

                    if (remainingDistance > 8)
                    {
                        speed = WinchSubsystem.UP_SPEED_MAX;
                    }
                    else if (remainingDistance < 2)
                    {
                        speed = 0.35;
                    }
                    else
                    {
                        speed = 0.45;
                    }


                    logger.trace("{}.execute() - raising elevator at a speed of {}; remainingDistance = {}; calling winchSubsystem.raise({}",
                                 getClass().getSimpleName(), speed, remainingDistance, speed);

                    winchSubsystem.raise(speed);
                }
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in WinchRaiseCommand.execute()", e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for WinchRaiseCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        final double currentHeight = winchSubsystem.getHeightInInches();
        final boolean finished = currentHeight >= targetHeight;
        if (finished)
        {
            logger.debug("{}.isFinished() -- currentHeight = {}  targetHeight = {}  finished = {}", getClass().getSimpleName(), currentHeight, targetHeight, finished);
        }
        else
        {
            logger.trace("{}.isFinished() -- currentHeight = {}  targetHeight = {}  finished = {}", getClass().getSimpleName(), currentHeight, targetHeight, finished);
        }
        return finished;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (winchSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        logger.debug("{}.end() called", getClass().getSimpleName());
        winchSubsystem.stop();
        //get ready for the next use
        isFirstCall = true;
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
