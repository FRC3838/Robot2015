package frc.team3838.robot2015.commands.driveTrain;


import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.team3838.robot2015.EM;



public class DriveTrainRecordAllDistancesCommandGroup extends CommandGroup
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveTrainRecordAllDistancesCommandGroup.class);


    public DriveTrainRecordAllDistancesCommandGroup()
    {
        super();
        logger.debug("{} constructor called", getClass().getSimpleName());
        addCommands();
    }


    public DriveTrainRecordAllDistancesCommandGroup(String name)
    {
        super(name);
        addCommands();
    }


    protected void addCommands()
    {

        if (EM.isDriveTrainSubsystemEnabled)
        {
            logger.debug("Starting {}.addCommands()", getClass().getSimpleName());
            addParallel(new DriveTrainRecordAverageDistance());
            addParallel(new DriveTrainRecordIndividualDistances());
            logger.debug("Exiting {}.addCommands()", getClass().getSimpleName());
        }
    }

    // In a constructor...
    // Add Commands:
    // e.g. addSequential(new Command1());
    //      addSequential(new Command2());
    // these will run in order.

    // To run multiple commands at the same time,
    // use addParallel()
    // e.g. addParallel(new Command1());
    //      addSequential(new Command2());
    // Command1 and Command2 will run in parallel.

    // A command group will require all of the subsystems that each member
    // would require.
    // e.g. if Command1 requires chassis, and Command2 requires arm,
    // a CommandGroup containing them would require both the chassis and the
    // arm.
}