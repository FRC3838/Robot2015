package frc.team3838.robot2015.commands.driveTrain.drive;

import frc.team3838.robot2015.commands.CommandBase;
import frc.team3838.robot2015.subsystems.drive.DriveTrainSubsystem.Direction;



public class DriveSetDistance extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveSetDistance.class);

    private final Direction direction;
    private final double distanceToTravelInInches;


    public static DriveSetDistance inches(Direction direction, double distanceToTravelInInches)
    {
        return new DriveSetDistance(direction, distanceToTravelInInches);
    }

    public static DriveSetDistance feet(Direction direction, double distanceToTravelInFeet)
    {
        return new DriveSetDistance(direction, distanceToTravelInFeet * 12);
    }


    /**
     *
     * @param distanceToTravel the distance to travel written as {@code 6"} of inches and {@code 3'} for feet
     * @return a configured DriveSetDistance command
     */
    public static DriveSetDistance forward(String distanceToTravel)
    {
        distanceToTravel = distanceToTravel.trim();
        final char unit = distanceToTravel.charAt(distanceToTravel.length() - 1);
        if (unit == '\'')
        {
            return new DriveSetDistance(Direction.FORWARD, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)) * 12);
        }
        else if(unit == '"')
        {
            return new DriveSetDistance(Direction.FORWARD, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)));
        }
        else
        {
            throw new IllegalArgumentException("Could not parse " + distanceToTravel + " as a distance in inches or feet.");
        }
    }

    public static DriveSetDistance forwardInches(double distanceToTravelInInches)
    {
        return new DriveSetDistance(Direction.FORWARD, distanceToTravelInInches);
    }

    public static DriveSetDistance forwardFeet(double distanceToTravelInFeet)
    {
        return new DriveSetDistance(Direction.FORWARD, distanceToTravelInFeet * 12);
    }


    /**
     * @param distanceToTravel the distance to travel written as {@code 6"} of inches and {@code 3'} for feet
     *
     * @return a configured DriveSetDistance command
     */
    public static DriveSetDistance reverse(String distanceToTravel)
    {
        distanceToTravel = distanceToTravel.trim();
        final char unit = distanceToTravel.charAt(distanceToTravel.length() - 1);
        if (unit == '\'')
        {
            return new DriveSetDistance(Direction.REVERSE, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)) * 12);
        }
        else if (unit == '"')
        {
            return new DriveSetDistance(Direction.REVERSE, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)));
        }
        else
        {
            throw new IllegalArgumentException("Could not parse " + distanceToTravel + " as a distance in inches or feet.");
        }
    }

    public static DriveSetDistance reverseInches(double distanceToTravelInInches)
    {
        return new DriveSetDistance(Direction.REVERSE, distanceToTravelInInches);
    }

    public static DriveSetDistance reverseFeet(double distanceToTravelInFeet)
    {
        return new DriveSetDistance(Direction.REVERSE, distanceToTravelInFeet * 12);
    }


    /**
     * @param distanceToTravel the distance to travel written as {@code 6"} of inches and {@code 3'} for feet
     *
     * @return a configured DriveSetDistance command
     */
    public static DriveSetDistance left(String distanceToTravel)
    {
        distanceToTravel = distanceToTravel.trim();
        final char unit = distanceToTravel.charAt(distanceToTravel.length() - 1);
        if (unit == '\'')
        {
            return new DriveSetDistance(Direction.LEFT, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)) * 12);
        }
        else if (unit == '"')
        {
            return new DriveSetDistance(Direction.LEFT, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)));
        }
        else
        {
            throw new IllegalArgumentException("Could not parse " + distanceToTravel + " as a distance in inches or feet.");
        }
    }


    public static DriveSetDistance leftInches(double distanceToTravelInInches)
    {
        return new DriveSetDistance(Direction.LEFT, distanceToTravelInInches);
    }

    public static DriveSetDistance leftFeet(double distanceToTravelInFeet)
    {
        return new DriveSetDistance(Direction.LEFT, distanceToTravelInFeet * 12);
    }


    /**
     * @param distanceToTravel the distance to travel written as {@code 6"} of inches and {@code 3'} for feet
     *
     * @return a configured DriveSetDistance command
     */
    public static DriveSetDistance right(String distanceToTravel)
    {
        distanceToTravel = distanceToTravel.trim();
        final char unit = distanceToTravel.charAt(distanceToTravel.length() - 1);
        if (unit == '\'')
        {
            return new DriveSetDistance(Direction.RIGHT, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)) * 12);
        }
        else if (unit == '"')
        {
            return new DriveSetDistance(Direction.RIGHT, Double.parseDouble(distanceToTravel.substring(0, distanceToTravel.length() - 1)));
        }
        else
        {
            throw new IllegalArgumentException("Could not parse " + distanceToTravel + " as a distance in inches or feet.");
        }
    }


    public static DriveSetDistance rightInches(double distanceToTravelInInches)
    {
        return new DriveSetDistance(Direction.RIGHT, distanceToTravelInInches);
    }

    public static DriveSetDistance rightFeet(double distanceToTravelInFeet)
    {
        return new DriveSetDistance(Direction.RIGHT, distanceToTravelInFeet * 12);
    }



    public DriveSetDistance(Direction direction, double distanceToTravelInInches)
    {
        requires(driveTrainSubsystem);
        this.distanceToTravelInInches = distanceToTravelInInches;
        this.direction = direction;
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            driveTrainSubsystem.reset();
        }
        else
        {
            logger.info("Not all required subsystems for DriveSetDistance are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    protected double getDistanceReadingAbsoluteValue()
    {
        switch (direction)
        {
            case FORWARD:
            case REVERSE:
                return Math.abs(driveTrainSubsystem.getAverageDistanceInInches());
            case RIGHT:
            case LEFT:
                return Math.abs(driveTrainSubsystem.getSideMovementDistance());
        }
        return Double.MAX_VALUE;
    }


    protected double getRemainingDistance()
    {
        return distanceToTravelInInches - getDistanceReadingAbsoluteValue();
    }


    protected boolean hasReachTarget()
    {
        return getDistanceReadingAbsoluteValue() >= distanceToTravelInInches;
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {

                if (hasReachTarget())
                {
                    driveTrainSubsystem.stop();
                }
                else
                {
                    double speed = .6;

                    final double remainingDistance = getRemainingDistance();
                    if (remainingDistance > 36)
                    {
                        speed = 0.65;
                    }
                    else if (remainingDistance < 6)
                    {
                        speed = 0.25;
                    }
                    else
                    {
                        speed = 0.5;
                    }
//                    else if (remainingDistance > 12)
//                    {
//                        speed = 0.50;
//                    }
//                    else if (remainingDistance > 8)
//                    {
//                        speed = 0.25;
//                    }
//                    else if (remainingDistance < 3)
//                    {
//                        speed = 0.10;
//                    }
//                    else if (remainingDistance <= 0)
//                    {
//                        speed = 0;
//                    }
                    logger.trace("Speed set to {}", speed);
                    driveTrainSubsystem.drive(direction, speed);
                }
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in DriveSetDistance.execute()", e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for DriveSetDistance are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return hasReachTarget();
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (driveTrainSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        driveTrainSubsystem.stop();
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
