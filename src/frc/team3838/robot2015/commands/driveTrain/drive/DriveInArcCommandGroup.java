package frc.team3838.robot2015.commands.driveTrain.drive;


import edu.wpi.first.wpilibj.command.CommandGroup;



public class DriveInArcCommandGroup extends CommandGroup
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveInArcCommandGroup.class);

    private boolean isFirstCall = true;

    public DriveInArcCommandGroup()
    {
        super();
        addCommands();
    }


    public DriveInArcCommandGroup(String name)
    {
        super(name);
        addCommands();
    }


    protected void addCommands()
    {
        /*
            X: 0.000  Y:  0.024   for 2.4 seconds    (36.050 - 33.651)
            X: 0.024  Y: -0.225  for 1.13 seconds   (38.590 - 37.372)
            X: 0.000  Y: -0.227  for 0.32 seconds   (38.912 - 38.590)


            Part D
            X: 0.000  Y: -0.211  Z: -0.65 for 0.14 seconds  (39.071 - 38.931)
            Part E
            X: 0.000  Y: -0.192  Z: -0.109 for 0.08 seconds
            Part F
            X: 0.000  Y: -0.164  Z: -0.145 for 0.257 seconds


            Part G
            X: -0.008  Y: -0.08 Z: -0.273  for 0.3 seconds
            Part H
            X: -0.008   Y: -0.031   Z: -0.328 for .36 seconds
         */


        // @formatter:off
        //                                   X       Y       Z             seconds
//        addSequential(new DriveXyzCommand( 0.024, -0.225,  0.000,  false,  1.13 ));
//        addSequential(new DriveXyzCommand( 0.000, -0.227,  0.000,  false,  0.32 ));
        addSequential(new DriveXyzCommand( 0.000, -0.211, -0.650,  false,  0.14 ));

        addSequential(new DriveXyzCommand( 0.000, -0.192, -0.109,  false,  0.08 ));
        addSequential(new DriveXyzCommand( 0.000, -0.134, -0.145,  false,  0.257));

        addSequential(new DriveXyzCommand(-0.008, -0.080, -0.273,  false,  0.3  ));
        addSequential(new DriveXyzCommand(-0.008, -0.031, -0.328,  true,   0.36 ));
        addSequential(new DriveXyzCommand( 0.024, -0.225,  0.000,  false,  1.13 ));
        addSequential(new DriveXyzCommand( 0.000, -0.227,  0.000,  false,  0.32 ));

         // @formatter:on

    }


    @Override
    protected void execute()
    {
        if (isFirstCall)
        {
            logger.info("{} starting execution", getClass().getSimpleName());
            isFirstCall = false;
        }
        super.execute();
    }


    @Override
    protected void end()
    {
        super.end();
        isFirstCall = true;
    }

    // In a constructor...
    // Add Commands:
    // e.g. addSequential(new Command1());
    //      addSequential(new Command2());
    // these will run in order.

    // To run multiple commands at the same time,
    // use addParallel()
    // e.g. addParallel(new Command1());
    //      addSequential(new Command2());
    // Command1 and Command2 will run in parallel.

    // A command group will require all of the subsystems that each member
    // would require.
    // e.g. if Command1 requires chassis, and Command2 requires arm,
    // a CommandGroup containing them would require both the chassis and the
    // arm.
}