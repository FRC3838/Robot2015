package frc.team3838.robot2015.commands.driveTrain.drive;

import frc.team3838.robot2015.commands.CommandBase;



public class DriveForwardSetDistance extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveForwardSetDistance.class);

    private final double targetDistanceInInches;
    private double endDistanceInInches;

    public DriveForwardSetDistance(double targetDistanceInInches)
    {
        requires(driveTrainSubsystem);
        this.targetDistanceInInches = targetDistanceInInches;
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            endDistanceInInches = driveTrainSubsystem.getAverageDistanceInInches() + targetDistanceInInches;
            logger.info("DriveForwardSetDistance with an end distance of {}", endDistanceInInches);
        }
        else
        {
            logger.info("Not all required subsystems for DriveSetDistance are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                double speed = 1;
                final double remainingDistance = endDistanceInInches - driveTrainSubsystem.getAverageDistanceInInches();
                if (remainingDistance > 36)
                {
                    speed = 1;
                }
                else if (remainingDistance > 18)
                {
                    speed = 0.75;
                }
                else if (remainingDistance > 12)
                {
                    speed = 0.50;
                }
                else if (remainingDistance > 8)
                {
                    speed = 0.25;
                }
                else if (remainingDistance < 3)
                {
                    speed = 0.10;
                }
                else if (remainingDistance <=0)
                {
                    speed = 0;
                }

                logger.info("Speed set to {}%", speed);
                driveTrainSubsystem.driveForward(speed);
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in DriveSetDistance.execute()", e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for DriveSetDistance are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return driveTrainSubsystem.getAverageDistanceInInches() >= endDistanceInInches;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (driveTrainSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        driveTrainSubsystem.stop();
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }
}
