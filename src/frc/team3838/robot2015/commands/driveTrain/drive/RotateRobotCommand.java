package frc.team3838.robot2015.commands.driveTrain.drive;

import frc.team3838.robot2015.commands.CommandBase;
import frc.team3838.robot2015.subsystems.drive.DriveTrainSubsystem.RotationDirection;



public class RotateRobotCommand extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RotateRobotCommand.class);

    private final double degreesToRotate;

    private final RotationDirection rotationDirection;

    private boolean isFirstCall = true;


    /*
        SOME IMPLEMENTATION  NOTES
            1) The gyro is not like a compass, it does not go from 0 to 360 and then cycle back.
               If you keep rotating it will to past 360 in the 400, and onwards
            2) If you rotate CW, the gyro angle goes down (and into the negatives
            3) If you rotate CCW, the gyro angle goes up

     */

    public static RotateRobotCommand clockwise(double degreesToRotate)
    {
        return new RotateRobotCommand(degreesToRotate, RotationDirection.CLOCKWISE);
    }

    public static RotateRobotCommand counterclockwise(double degreesToRotate)
    {
        return new RotateRobotCommand(degreesToRotate, RotationDirection.COUNTERCLOCKWISE);
    }


    public static RotateRobotCommand clockwise45Degrees()
    {
        return new RotateRobotCommand(45, RotationDirection.CLOCKWISE);
    }

    public static RotateRobotCommand counterclockwise45Degrees()
    {
        return new RotateRobotCommand(45, RotationDirection.COUNTERCLOCKWISE);
    }

    public static RotateRobotCommand clockwise90Degrees()
    {
        return new RotateRobotCommand(90, RotationDirection.CLOCKWISE);
    }

    public static RotateRobotCommand counterclockwise90Degrees()
    {
        return new RotateRobotCommand(90, RotationDirection.COUNTERCLOCKWISE);
    }

    public static RotateRobotCommand clockwise180Degrees()
    {
        return new RotateRobotCommand(180, RotationDirection.CLOCKWISE);
    }

    public static RotateRobotCommand counterclockwise180Degrees()
    {
        return new RotateRobotCommand(180, RotationDirection.COUNTERCLOCKWISE);
    }


    public RotateRobotCommand(double degreesToRotate)
    {
        this(degreesToRotate, RotationDirection.CLOCKWISE);
    }

    public RotateRobotCommand(double degreesToRotate, RotationDirection rotationDirection)
    {
        requires(driveTrainSubsystem);
        this.rotationDirection = rotationDirection;
        this.degreesToRotate = degreesToRotate - 3;
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            driveTrainSubsystem.resetGyro();
            final double gyroAngle = driveTrainSubsystem.getGyroAngle();
            logger.info("At Initialization of {},  gyroAngle = {},  targetAngle = {} ", getClass().getSimpleName(), gyroAngle, degreesToRotate);
        }
        else
        {
            logger.info("Not all required subsystems for RotateRobotCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                if (isFirstCall)
                {
                    driveTrainSubsystem.resetGyro();
                    isFirstCall = false;
                }

                logger.debug("{}.execute called. Rotating {} to a target of {}", getClass().getSimpleName(), rotationDirection, degreesToRotate);
                if (isFinished())
                {
                    driveTrainSubsystem.stop();
                    logger.debug("{} is Finished", getClass().getSimpleName());
                }
                else
                {
                    logger.debug("Calling driveTrainSubsystem.rotate(...)");
                    final double degreesRemaining = degreesRemaining();

                    final double speed;
                    if (degreesRemaining > 15)
                    {
                        //Full speed ahead Scotty
                        speed = 0.45;
                    }
                    else if (degreesRemaining > 10)
                    {
                        speed = 0.40;
                    }
                    else if (degreesRemaining > 8)
                    {
                       speed = 0.35;
                    }
                    else if (degreesRemaining > 5)
                    {
                        speed = 0.3;
                    }
                    else
                    {
                        speed = 0.25;
                    }
                    driveTrainSubsystem.rotate(rotationDirection, speed);
                }
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in RotateRobotCommand.execute()", e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for RotateRobotCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        final double gyroAngle = Math.abs(driveTrainSubsystem.getGyroAngle());
        boolean finished = (gyroAngle >= degreesToRotate - 1);
        logger.debug("{} isFinished Check: gyroAngle = {}, targetAngle = {}  isFinished = {}", getClass().getSimpleName(), gyroAngle, degreesToRotate, finished);
        return finished;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (driveTrainSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        //Prep it for the next time the command is executed
        isFirstCall = true;
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }

    protected double degreesRemaining()
    {
        return degreesToRotate - Math.abs(driveTrainSubsystem.getGyroAngle());
    }
}
