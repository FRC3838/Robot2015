package frc.team3838.robot2015.commands.driveTrain.drive;

import java.util.concurrent.TimeUnit;

import frc.team3838.robot2015.commands.CommandBase;



public class DriveXyzCommand extends CommandBase
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DriveXyzCommand.class);

    private final double x;
    private final double y;
    private final double z;
    private final long ms;
    private final boolean stopAtEnd;

    private long startTime;
    private boolean finished = false;
    private boolean isFirstCall = true;



    public DriveXyzCommand(double x, double y, double z, boolean stopAtEnd, long duration, TimeUnit timeUnit)
    {
        this(x, y, z, stopAtEnd, timeUnit.toMillis(duration));
    }


    public DriveXyzCommand(double x, double y, double z, boolean stopAtEnd, long duration1, TimeUnit timeUnit1, long duration2, TimeUnit timeUnit2)
    {
        this.ms = (timeUnit1.toMillis(duration1) + timeUnit2.toMillis(duration2));
        this.x = x;
        this.y = y;
        this.z = z;
        this.stopAtEnd = stopAtEnd;
        requires(driveTrainSubsystem);
    }


    public DriveXyzCommand(double x, double y, double z, boolean stopAtEnd, long duration1, TimeUnit timeUnit1, long duration2, TimeUnit timeUnit2,
                           long duration3,
                           TimeUnit timeUnit3)
    {
        this.ms = (timeUnit1.toMillis(duration1) + timeUnit2.toMillis(duration2) + timeUnit3.toMillis(duration3));
        this.x = x;
        this.y = y;
        this.z = z;
        this.stopAtEnd = stopAtEnd;
        requires(driveTrainSubsystem);
    }


    public DriveXyzCommand(double x, double y, double z, boolean stopAtEnd, long ms)
    {
        this.ms = ms;
        this.x = x;
        this.y = y;
        this.z = z;
        this.stopAtEnd = stopAtEnd;
        requires(driveTrainSubsystem);
    }

    public DriveXyzCommand(double x, double y, double z, boolean stopAtEnd, double seconds)
    {
        this.ms = (long) (seconds * 1000);
        this.x = x;
        this.y = y;
        this.z = z;
        this.stopAtEnd = stopAtEnd;
        requires(driveTrainSubsystem);
    }


    // Called just before this Command runs the first time
    @Override
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            // No op
        }
        else
        {
            logger.info("Not all required subsystems for {} are enabled. The command can not be and will not be initialized or executed.", getClass().getSimpleName());
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    @Override
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                if (isFirstCall)
                {
                    startTimer();
                    isFirstCall = false;
                }
                long diff = System.currentTimeMillis() - startTime;
                finished = diff >= ms;
                if (!finished)
                {
                    driveTrainSubsystem.driveViaMecanum(x, y, z);
                }
                else if (stopAtEnd)
                {
                    driveTrainSubsystem.stop();
                }
            }
            catch (Exception e)
            {
                logger.error("An exception occurred in {}.execute()", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.trace("Not all required subsystems for {} are enabled. The command can not be and will not be executed.", getClass().getSimpleName());
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished()
    {
        return finished;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (driveTrainSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    @Override
    protected void end()
    {
        if (stopAtEnd)
        {
            driveTrainSubsystem.stop();
        }
        //reset for the next run
        isFirstCall = true;
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted()
    {

    }


    private void startTimer()
    {
        finished = false;
        startTime = System.currentTimeMillis();
    }
}
