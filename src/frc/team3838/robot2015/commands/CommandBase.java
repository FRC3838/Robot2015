package frc.team3838.robot2015.commands;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.robot2015.OI2015;
import frc.team3838.robot2015.subsystems.WinchSubsystem;
import frc.team3838.robot2015.subsystems.camera.UsbCameraSubsystem;
import frc.team3838.robot2015.subsystems.claw.ClawSubsystem;
import frc.team3838.robot2015.subsystems.drive.DriveTrainSubsystem;



/**
 * The base for all commands. All atomic commands should subclass CommandBase.
 * CommandBase stores creates and stores each control system. To access a
 * subsystem elsewhere in your code in your code use CommandBase.exampleSubsystem
 *
 * @author Author
 */
public abstract class CommandBase extends Command
{
    protected static final ClawSubsystem clawSubsystem = ClawSubsystem.getInstance();
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger loggerCommandBase = LoggerFactory.getLogger(CommandBase.class);

    public static OI2015 oiInstance;
    // Create a single *static* instance of all of your subsystems
    protected static final WinchSubsystem winchSubsystem = WinchSubsystem.getInstance();
    protected static final DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();
    protected static final UsbCameraSubsystem usbCameraSubsystem = UsbCameraSubsystem.getInstance();

    /** @noinspection ConstructorNotProtectedInAbstractClass */
    public CommandBase(String name)
    {
        super(name);
    }


    /** @noinspection ConstructorNotProtectedInAbstractClass */
    public CommandBase()
    {
        super();
    }


    public static void init()
    {
        loggerCommandBase.debug("Entering CommandBase.init()");
        loggerCommandBase.info("Initializing CommandBase (i.e. CommandBase.init()");

        /*

            Typically, the OI should be the very first thing initialized

         */

        if (oiInstance == null)
        {
            loggerCommandBase.debug("Calling OI constructor");
            // This MUST be here. If the OI2014 creates Commands (which it very likely
            // will), constructing it during the construction of CommandBase (from
            // which commands extend), subsystems are not guaranteed to be
            // yet. Thus, their requires() statements may grab null pointers. Bad
            // news. Don't move it.
            try
            {
                oiInstance = new OI2015();
            }
            catch (Exception e)
            {
                loggerCommandBase.error(">>>FATAL<<< Could not construct the OI instance in CommandBase.init(): {}", e.toString(), e);
            }
            loggerCommandBase.trace("Returned from OI Constructor call");
        }
        else
        {
            loggerCommandBase.info("oiInstance was already created. Was CommandBase.init() called multiple times????");
        }

        //ensure Buttons class has been initialized
//        loggerCommandBase.debug("Number of configured/assigned buttons: " + Buttons.getNumberOfAssignedButtons());

        // Show what command your subsystem is running on the SmartDashboard
        //SmartDashboard.putData(exampleSubsystem);


        // **NOTE: We create our buttons in the Buttons class.

        loggerCommandBase.trace("Exiting CommandBase.init()");
    }
}
