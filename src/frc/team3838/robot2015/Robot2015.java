package frc.team3838.robot2015;

import java.io.BufferedInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import frc.team3838.robot2015.commands.CommandBase;
import frc.team3838.robot2015.commands.autonomous.AutonomousSelector;
import frc.team3838.robot2015.subsystems.drive.DriveTrainSubsystem;
import frc.team3838.utils.LogbackProgrammaticConfiguration;



/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
@SuppressWarnings("RefusedBequest")
public class Robot2015 extends IterativeRobot
{

    @SuppressWarnings("UnusedDeclaration")
    private static Logger logger;

    private Command autonomousCommand;

    private boolean bootCompleteMsgHasBeenLogged = false;

    @SuppressWarnings("FieldCanBeLocal")
    private static String rioName = "<undetermined>";


    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     */
    @Override
    public void robotInit()
    {
        LogbackProgrammaticConfiguration.configure();
        logger = LoggerFactory.getLogger(Robot2015.class);
        determineAndSetRioName();
        AutonomousSelector.setupChooser();

        bootCompleteMsgHasBeenLogged = false;
        // Initialize all subsystems
        CommandBase.init();
        //OI is initialized in the CommandBase class

        // instantiate the command used for the autonomous period
        //autonomousCommand = new ExampleCommand();
    }


    @Override
    public void disabledPeriodic()
    {
        Scheduler.getInstance().run();
    }


    @Override
    public void autonomousInit()
    {
        // schedule the autonomous command (example)

        try
        {
            if (EM.isDriveTrainSubsystemEnabled)
            {
                DriveTrainSubsystem.getInstance().resetGyro();
                DriveTrainSubsystem.getInstance().disableMotorSafetyForAutonomousMode();
                DriveTrainSubsystem.getInstance().setUseCurve(false);
            }
            autonomousCommand = AutonomousSelector.selectCommand();
            if (autonomousCommand != null) autonomousCommand.start();
        }
        catch (Exception e)
        {
            logger.error("An exception occurred in {}.autonomousInit()", getClass().getSimpleName(), e);
        }
    }


    /**
     * This function is called periodically during autonomous
     */
    @Override
    public void autonomousPeriodic()
    {
        Scheduler.getInstance().run();
    }


    @Override
    public void teleopInit()
    {
        // This makes sure that the autonomous stops running when
        // teleop starts running. If you want the autonomous to
        // continue until interrupted by another command, remove
        // this line or comment it out.
        if (autonomousCommand != null) autonomousCommand.cancel();
        try
        {
            DriveTrainSubsystem.getInstance().enableMotorSafetyPostAutonomousMode();
            DriveTrainSubsystem.getInstance().setUseCurve(true);
        }
        catch (Exception e) { logger.error("An exception occurred in {}.teleopInit()", getClass().getSimpleName(), e); }


        //TODO: Do we want to do this
        // ClawSubsystem.getInstance().openClaw();
    }


    /**
     * This function is called when the disabled button is hit.
     * You can use it to reset subsystems before shutting down.
     */
    @Override
    public void disabledInit()
    {
        logger.info("Robot entering disabled mode");

        if (!bootCompleteMsgHasBeenLogged)
        {
            bootCompleteMsgHasBeenLogged = true;
            logger.info("");
            logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            logger.info("~ ROBOT BOOT UP COMPLETED ~");
            logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            logger.info("");
        }
    }


    /**
     * This function is called periodically during operator control
     */
    @Override
    public void teleopPeriodic()
    {
        Scheduler.getInstance().run();
    }


    /**
     * This function is called periodically during test mode
     */
    @Override
    public void testPeriodic()
    {
        LiveWindow.run();
    }

    private void determineAndSetRioName()
    {
        Runtime run = Runtime.getRuntime();
        Process process;
        try
        {
            //noinspection CallToRuntimeExec
            process = run.exec("hostname");
            try (final BufferedInputStream in = new BufferedInputStream(process.getInputStream()))
            {

                byte[] b = new byte[256];
                //noinspection ResultOfMethodCallIgnored
                in.read(b, 0, 256);
                rioName = new String(b).trim();
                logger.info("roboRIO name = {}", rioName);
            }
        }
        catch (Exception e)
        {
            logger.warn("Could not determine roboRIO name.", e);
        }
    }


    @SuppressWarnings("UnusedDeclaration")
    public static String getRioName()
    {
        return rioName;
    }
}
