package frc.team3838.robot2015.subsystems.drive;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;
import frc.team3838.utils.MathUtils;
import frc.team3838.utils.enums.FRQuadrant;
import frc.team3838.utils.enums.SideQuadrant;



@SuppressWarnings("UnusedDeclaration")
public class Wheel
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger logger = LoggerFactory.getLogger(Wheel.class);

    public static final double DEFAULT_WHEEL_DIAMETER_IN_INCHES = 6.0;
    public static final int DEFAULT_ENCODE_PULSES_PER_REVOLUTION = 250;

    private final RobotDrive.MotorType motorType;
    private final SpeedController speedController;
    private final Encoder encoder;

    private final SideQuadrant side;
    private final FRQuadrant frQuadrant;

    private final double wheelDiameterInInches;
    private final int encoderPulsesPerRevolution;
    private final double distancePerPulse;

    private final int index;


    public Wheel(RobotDrive.MotorType motorType,
                 int speedControllerPwmChannel,
                 int encoderChannelA,
                 int encoderChannelB
    )

    {
        this(motorType, speedControllerPwmChannel, encoderChannelA, encoderChannelB, DEFAULT_WHEEL_DIAMETER_IN_INCHES, DEFAULT_ENCODE_PULSES_PER_REVOLUTION);
    }

    public Wheel(RobotDrive.MotorType motorType,
                 int speedControllerPwmChannel,
                 int encoderChannelA,
                 int encoderChannelB,
                 double wheelDiameterInInches,
                 int encoderPulsesPerRevolution
                 )
    {
        this.motorType = motorType;

        // static final int kFrontLeft_val = 0;
        // static final int kFrontRight_val = 1;
        // static final int kRearLeft_val = 2;
        // static final int kRearRight_val = 3;

        if (motorType.value == RobotDrive.MotorType.kFrontLeft.value)
        {
            frQuadrant = FRQuadrant.FRONT;
            side = SideQuadrant.LEFT;
            index = 0;
        }
        else if (motorType.value == RobotDrive.MotorType.kFrontRight.value)
        {
            frQuadrant = FRQuadrant.FRONT;
            side = SideQuadrant.RIGHT;
            index = 1;
        }
        else if (motorType.value == RobotDrive.MotorType.kRearLeft.value)
        {
            frQuadrant = FRQuadrant.REAR;
            side = SideQuadrant.LEFT;
            index = 2;
        }
        else if (motorType.value == RobotDrive.MotorType.kRearRight.value)
        {
            frQuadrant = FRQuadrant.REAR;
            side = SideQuadrant.RIGHT;
            index = 3;
        }
        else
        {
            final String msg = "Unknown MotorType with a value of " + motorType.value;
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }

        this.wheelDiameterInInches = wheelDiameterInInches;
        this.encoderPulsesPerRevolution = encoderPulsesPerRevolution;

        speedController = new Jaguar(speedControllerPwmChannel);
        encoder = new Encoder(encoderChannelA, encoderChannelB);
        // encoder.setDistancePerPulse((3.0 * Math.PI) / PULSES_PER_REVOLUTION);
        distancePerPulse = (this.wheelDiameterInInches * Math.PI) / encoderPulsesPerRevolution;
        encoder.setDistancePerPulse(distancePerPulse);
    }


    public int getIndex() { return index; }


    public void stop() { speedController.set(0); }


    public void setSpeed(double speed)
    {
        logger.info("Setting to targetSpeed of {}", speed);
        speedController.set(speed);
    }

    public void setForward05Percent() { setSpeed(0.05); }


    public void setForward10Percent() { setSpeed(0.10); }


    public void setForward15Percent() { setSpeed(0.15); }


    public void setForward20Percent() { setSpeed(0.20); }


    public void setForward25Percent() { setSpeed(0.25); }


    public void setForward30Percent() { setSpeed(0.30); }


    public void setForward35Percent() { setSpeed(0.35); }


    public void setForward40Percent() { setSpeed(0.40); }


    public void setForward45Percent() { setSpeed(0.45); }


    public void setForward50Percent() { setSpeed(0.50); }


    public void setForward55Percent() { setSpeed(0.55); }


    public void setForward60Percent() { setSpeed(0.60); }


    public void setForward65Percent() { setSpeed(0.65); }


    public void setForward70Percent() { setSpeed(0.70); }


    public void setForward75Percent() { setSpeed(0.75); }


    public void setForward80Percent() { setSpeed(0.80); }


    public void setForward85Percent() { setSpeed(0.85); }


    public void setForward90Percent() { setSpeed(0.90); }


    public void setForward95Percent() { setSpeed(0.95); }


    public void setForwardAsPercent(int percentage)
    {
        if (percentage < 0)
        {
            logger.error("Received forward speed is less than 0%. Setting to 0&");
            percentage = 0;
        }
        else if (percentage > 100)
        {
            logger.error("Received forward speed is greater than 100%. Setting to 100%");
            percentage = 100;
        }
        double targetSpeed = percentage / 100;
        setSpeed(targetSpeed);
    }

    public void setForwardFull() { setSpeed(1.0); }


    public void setReverse05Percent() { setSpeed(-0.05); }


    public void setReverse10Percent() { setSpeed(-0.10); }


    public void setReverse15Percent() { setSpeed(-0.15); }


    public void setReverse20Percent() { setSpeed(-0.20); }


    public void setReverse25Percent() { setSpeed(-0.25); }


    public void setReverse30Percent() { setSpeed(-0.30); }


    public void setReverse35Percent() { setSpeed(-0.35); }


    public void setReverse40Percent() { setSpeed(-0.40); }


    public void setReverse45Percent() { setSpeed(-0.45); }


    public void setReverse50Percent() { setSpeed(-0.50); }


    public void setReverse55Percent() { setSpeed(-0.55); }


    public void setReverse60Percent() { setSpeed(-0.60); }


    public void setReverse65Percent() { setSpeed(-0.65); }


    public void setReverse70Percent() { setSpeed(-0.70); }


    public void setReverse75Percent() { setSpeed(-0.75); }


    public void setReverse80Percent() { setSpeed(-0.80); }


    public void setReverse85Percent() { setSpeed(-0.85); }


    public void setReverse90Percent() { setSpeed(-0.90); }


    public void setReverse95Percent() { setSpeed(-0.95); }


    public void setReverseAsPercent(int percentage)
    {
        percentage = Math.abs(percentage);
        if (percentage < 0)
        {
            logger.error("Received reverse speed is less than 0%. Setting to 0%");
            percentage = 0;
        }
        else if (percentage > 100)
        {
            logger.error("Received revers speed is greater than 100%. Setting to 100%");
            percentage = 100;
        }
        double targetSpeed = percentage / 100;
        setSpeed(-targetSpeed);
    }


    public void setReverseFull() { setSpeed(-1.0); }


    public String increaseForwardSpeedOnePercent() { return increaseForwardSpeed(.01); }


    public String increaseForwardSpeedThreePercent() { return increaseForwardSpeed(.03); }


    public String increaseForwardSpeedFivePercent() { return increaseForwardSpeed(.05); }


    public String increaseForwardSpeedTenPercent() { return increaseForwardSpeed(.1); }


    public String increaseForwardSpeedFifteenPercent() { return increaseForwardSpeed(.15); }


    public String increaseForwardSpeedTwentyPercent() { return increaseForwardSpeed(.20); }


    public String increaseForwardSpeedTwentyFivePercent() { return increaseForwardSpeed(.25); }


    public String increaseForwardSpeedThirtyPercent() { return increaseForwardSpeed(.30); }


    public String increaseForwardSpeedThirtyFivePercent() { return increaseForwardSpeed(.35); }


    public String increaseForwardSpeedFortyPercent() { return increaseForwardSpeed(.40); }


    public String increaseForwardSpeedFortyFivePercent() { return increaseForwardSpeed(.45); }


    public String increaseForwardSpeedFiftyPercent() { return increaseForwardSpeed(.50); }


    public String increaseForwardSpeedSeventyFivePercent() { return increaseForwardSpeed(.75); }


    public String decreaseForwardSpeedOnePercent() { return decreaseForwardSpeed(.01); }


    public String decreaseForwardSpeedThreePercent() { return decreaseForwardSpeed(.03); }


    public String decreaseForwardSpeedFivePercent() { return decreaseForwardSpeed(.05); }


    public String decreaseForwardSpeedTenPercent() { return decreaseForwardSpeed(.1); }


    public String decreaseForwardSpeedFifteenPercent() { return decreaseForwardSpeed(.15); }


    public String decreaseForwardSpeedTwentyPercent() { return decreaseForwardSpeed(.20); }


    public String decreaseForwardSpeedTwentyFivePercent() { return decreaseForwardSpeed(.25); }


    public String decreaseForwardSpeedThirtyPercent() { return decreaseForwardSpeed(.30); }


    public String decreaseForwardSpeedThirtyFivePercent() { return decreaseForwardSpeed(.35); }


    public String decreaseForwardSpeedFortyPercent() { return decreaseForwardSpeed(.40); }


    public String decreaseForwardSpeedFortyFivePercent() { return decreaseForwardSpeed(.45); }


    public String decreaseForwardSpeedFiftyPercent() { return decreaseForwardSpeed(.50); }


    public String decreaseForwardSpeedSeventyFivePercent() { return decreaseForwardSpeed(.75); }


    /**
     * Increases the forward speed by the specified amount, but does nto allow the motor to go into reverse. For example, if the current speed is 5% (i.e. 0.05) forward and a value
     * of change of 8% (i.e. 0.08) is passed in, the motor speed will be set to 0, not 3% reversed.
     *
     * @param delta the amount to decrease as a value between 0 and 1.
     *
     * @return the new speed
     */
    public String decreaseForwardSpeed(double delta)
    {
        return modifySpeed(-Math.abs(delta));
    }


    public String increaseForwardSpeed(double delta) { return modifySpeed(Math.abs(delta));}


    public boolean isRunning() { return speedController.get() != 0; }


    public boolean isStopped()
    {
        return !isRunning();
    }


    public boolean isRunningFullSpeed()
    {
        return Math.abs(speedController.get()) >= 1.0D;
    }


    private String modifySpeed(double delta)
    {
        int inverseFactor = speedController.get() < 0 ? -1 : 1;
        double realDelta = delta * inverseFactor;
        double current = speedController.get();
        double newSpeed = current + realDelta;

        setSpeed(constrainValue(newSpeed));
        return MathUtils.toPercentage(speedController.get());
    }


    public double getSpeed()
    {
        return speedController.get();
    }


    public double getAbsoluteSpeed()
    {
        return Math.abs(speedController.get());
    }


    /**
     * Constrains the the speed to between -1 and 1. Note that this method does <b>not</b> set the speed field or modify the motor's speed. It is simply a math helper method.
     *
     * @param speed the speed to constrain
     *
     * @return the constrained speed
     */
    public double constrainValue(double speed)
    {
        if (speed > 1)
        {
            return 1;
        }
        else if (speed < -1)
        {
            return -1;
        }
        else
        {
            return speed;
        }
    }

    public void resetEncoder() { encoder.reset(); }

    public RobotDrive.MotorType getMotorType() { return motorType; }

    public SideQuadrant getSide() { return side; }

    public FRQuadrant getFrQuadrant() { return frQuadrant; }

    public double getDistanceInFeet() { return encoder.getDistance() / 12; }

    public double getDistanceInFeetRounded() { return MathUtils.roundToNPlaces(getDistanceInFeet(), 3); }

    public String getDistanceInFeetAsString() { return MathUtils.formatNumber(getDistanceInFeet(), 3); }

    public double getDistanceInInches() { return encoder.getDistance(); }

    public double getDistanceInInchesRounded() { return MathUtils.roundToNPlaces(getDistanceInInches(), 3); }

    public String getDistanceInInchesAsString() { return MathUtils.formatNumber(getDistanceInInches(), 3); }


    public double getWheelDiameterInInches()
    {
        return wheelDiameterInInches;
    }


    public double getDistancePerPulse()
    {
        return distancePerPulse;
    }


    public int getEncoderPulsesPerRevolution()
    {
        return encoderPulsesPerRevolution;
    }


    public SpeedController getSpeedController()
    {
        return speedController;
    }


    @Override
    public String toString()
    {
        return frQuadrant.name() + ' ' + side.name();
    }
}
