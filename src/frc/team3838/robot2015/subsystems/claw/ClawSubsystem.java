package frc.team3838.robot2015.subsystems.claw;


import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.robot2015.EM;
import frc.team3838.robot2015.config.PCM;
import frc.team3838.robot2015.config.SDBKeys;



public class ClawSubsystem extends Subsystem
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClawSubsystem.class);

    private static final DoubleSolenoid.Value openValue = Value.kForward;
    private static final DoubleSolenoid.Value closeValue = Value.kReverse;

    private String clawStatus = "OFF";

    private Boolean isOpen;


    @SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
    private Compressor compressor;
    private DoubleSolenoid rightSolenoid;
    private DoubleSolenoid leftSolenoid;

    /* ** SINGLETON NEEDS TO BE LAST FIELD DEFINED  ** */
    private static final ClawSubsystem singleton = new ClawSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ClawSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ClawSubsystem subsystem = new ClawSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     ClawSubsystem subsystem = ClawSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static ClawSubsystem getInstance() {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     ClawSubsystem subsystem = new ClawSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     ClawSubsystem subsystem = ClawSubsystem.getInstance();
     * </pre>
     */
    private ClawSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.debug("Initializing {}", getClass().getSimpleName());
                compressor = new Compressor();
                logger.info("Compressor has been initialized");

                rightSolenoid = new DoubleSolenoid(PCM.rightSolenoidA, PCM.rightSolenoidB);
                rightSolenoid.set(Value.kOff);
                leftSolenoid = new DoubleSolenoid(PCM.leftSolenoidA, PCM.leftSolenoidB);
                leftSolenoid.set(Value.kOff);

                sdbPutStatus();
                logger.debug("ClawSubsystem initialization completed successfully");
            }
            catch (Exception e)
            {
                EM.isClawSubsystemEnabled = false;
                logger.error("An exception occurred in {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.info("{}} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }

    public void openClaw()
    {
        rightSolenoid.set(openValue);
        leftSolenoid.set(openValue);
        clawStatus = "OPENED";
        isOpen = true;
        sdbPutStatus();
    }

    public void closeClaw()
    {
        rightSolenoid.set(closeValue);
        leftSolenoid.set(closeValue);
        clawStatus = "CLOSED";
        isOpen = false;
        sdbPutStatus();
    }

    public void stopClaw()
    {
        rightSolenoid.set(Value.kOff);
        leftSolenoid.set(Value.kOff);
        clawStatus = "OFF";
        sdbPutStatus();
    }

    public void toggleClaw()
    {
        if (isOpen !=null)
        {
            if (isOpen)
            {
                closeClaw();
            }
            else
            {
                openClaw();
            }
        }
    }

    @Override
    public void initDefaultCommand()
    {
        //setDefaultCommand(new MyCommand());
    }

    public void sdbPutStatus()
    {
        final String key = SDBKeys.CLAW_STATUS;
        logger.debug("{} : {}", key, clawStatus);
        SmartDashboard.putString(key, clawStatus);
    }


    /**
     * Stop the compressor from running in closed loop control mode.
     * Use the method in cases where you would like to manually stop and start the compressor
     * for applications such as conserving battery or making sure that the compressor motor
     * doesn't start during critical operations.
     */
    public void stopCompressor()
    {
        compressor.stop();
    }


    /**
     * Start the compressor running in closed loop control mode
     * Use the method in cases where you would like to manually stop and start the compressor
     * for applications such as conserving battery or making sure that the compressor motor
     * doesn't start during critical operations.
     */
    public void startCompressor()
    {
        compressor.start();
    }

    public boolean isEnabled()
    {
        return EM.isClawSubsystemEnabled;
    }
}
