package frc.team3838.robot2015.subsystems.camera;


import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.vision.USBCamera;
import frc.team3838.robot2015.EM;
import frc.team3838.wpiext.CameraServerModified;



public class UsbCameraSubsystem extends Subsystem
{
    @SuppressWarnings("UnusedDeclaration")
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(UsbCameraSubsystem.class);
    //the camera name (ex "cam0") can be found through the roborio web interface
    private static final String CAMERA_NAME = "cam0";


    @SuppressWarnings("FieldCanBeLocal")
    private CameraServerModified server;
    @SuppressWarnings("FieldCanBeLocal")
    private USBCamera camera;
    
    
    /* ** SINGLETON NEEDS TO BE LAST FIELD DEFINED  ** */
    private static final UsbCameraSubsystem singleton = new UsbCameraSubsystem();

    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new UsbCameraSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     UsbCameraSubsystem subsystem = new UsbCameraSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     UsbCameraSubsystem subsystem = UsbCameraSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static UsbCameraSubsystem getInstance() {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     UsbCameraSubsystem subsystem = new UsbCameraSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     UsbCameraSubsystem subsystem = UsbCameraSubsystem.getInstance();
     * </pre>
     */
    private UsbCameraSubsystem()
    {
        if (isEnabled())
        {
            try
            {
                logger.info("Initializing {}", getClass().getSimpleName());
                logger.info("Creating CameraServer");
                server = CameraServerModified.getInstance();
                //Set the quality of the compressed JPEG image sent to the dashboard - from 0 to 100
                server.setQuality(50);


                camera = new USBCamera(CAMERA_NAME);
                camera.openCamera();
//                camera.setSize(640, 480);
                camera.setSize(1280, 720);
                camera.updateSettings();

                server.startAutomaticCapture(camera);


                logger.info("{} initialization completed successfully", getClass().getSimpleName());
            }
            catch (Exception e)
            {
                EM.isUsbCameraSubsystemEnabled = false;
                logger.error("An exception occurred in the {} constructor", getClass().getSimpleName(), e);
            }
        }
        else
        {
            logger.info("{} is disabled and will not be initialized", getClass().getSimpleName());
        }
    }


    @Override
    public void initDefaultCommand()
    {
        //setDefaultCommand(new MyCommand());
    }


    public boolean isEnabled()
    {
        return EM.isUsbCameraSubsystemEnabled;
    }
}
