package frc.team3838.controls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.GenericHID;



public class AutonomousHID extends GenericHID
{
    private static final Logger logger = LoggerFactory.getLogger(AutonomousHID.class);


    @Override
    public double getX(Hand hand)
    {
        //TODO: Write this 'getX' implemented method in the 'AutonomousHID' class
        throw new IllegalStateException("The 'getX' method in the 'AutonomousHID' class is not yet implemented.");
        //return 0;
    }


    @Override
    public double getY(Hand hand)
    {
        //TODO: Write this 'getY' implemented method in the 'AutonomousHID' class
        throw new IllegalStateException("The 'getY' method in the 'AutonomousHID' class is not yet implemented.");
        //return 0;
    }


    @Override
    public double getZ(Hand hand)
    {
        //TODO: Write this 'getZ' implemented method in the 'AutonomousHID' class
        throw new IllegalStateException("The 'getZ' method in the 'AutonomousHID' class is not yet implemented.");
        //return 0;
    }


    @Override
    public double getTwist()
    {
        //TODO: Write this 'getTwist' implemented method in the 'AutonomousHID' class
        throw new IllegalStateException("The 'getTwist' method in the 'AutonomousHID' class is not yet implemented.");
        //return 0;
    }


    @Override
    public double getThrottle()
    {
        //TODO: Write this 'getThrottle' implemented method in the 'AutonomousHID' class
        throw new IllegalStateException("The 'getThrottle' method in the 'AutonomousHID' class is not yet implemented.");
        //return 0;
    }


    @Override
    public double getRawAxis(int which)
    {
        //TODO: Write this 'getRawAxis' implemented method in the 'AutonomousHID' class
        throw new IllegalStateException("The 'getRawAxis' method in the 'AutonomousHID' class is not yet implemented.");
        //return 0;
    }


    @Override
    public boolean getTrigger(Hand hand)
    {
        //TODO: Write this 'getTrigger' implemented method in the 'AutonomousHID' class
        throw new IllegalStateException("The 'getTrigger' method in the 'AutonomousHID' class is not yet implemented.");
        //return false;
    }


    @Override
    public boolean getTop(Hand hand)
    {
        //TODO: Write this 'getTop' implemented method in the 'AutonomousHID' class
        throw new IllegalStateException("The 'getTop' method in the 'AutonomousHID' class is not yet implemented.");
        //return false;
    }


    @Override
    public boolean getBumper(Hand hand)
    {
        //TODO: Write this 'getBumper' implemented method in the 'AutonomousHID' class
        throw new IllegalStateException("The 'getBumper' method in the 'AutonomousHID' class is not yet implemented.");
        //return false;
    }


    @Override
    public boolean getRawButton(int button)
    {
        //TODO: Write this 'getRawButton' implemented method in the 'AutonomousHID' class
        throw new IllegalStateException("The 'getRawButton' method in the 'AutonomousHID' class is not yet implemented.");
        //return false;
    }


    @Override
    public int getPOV(int pov)
    {
        //TODO: Write this 'getPOV' implemented method in the 'AutonomousHID' class
        throw new IllegalStateException("The 'getPOV' method in the 'AutonomousHID' class is not yet implemented.");
        //return 0;
    }
}
